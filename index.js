import express from 'express';
import one from 'onecolor';

const app = express();

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.get('/', (req, res) => {
  res.json({ response: 'Hello, World!' });
});

app.get('/task2a', (req, res) => {
  const a = !isNaN(req.query.a) && +req.query.a;
  const b = !isNaN(req.query.b) && +req.query.b;

  res.send(`${a + b}`);
});

app.get('/task2b', (req, res) => {
  const fullname = req.query.fullname.toUpperCase() || '';
  let initials = fullname ? fullname.split(' ').filter(el => el) : 0;
  let shortname = 'Invalid fullname';
  let lastname;

  if (initials && initials.length <= 3 && !/[\d_/]+/.test(fullname)) {
    lastname = initials.pop();
    lastname = lastname[0] + lastname.slice(1).toLowerCase();
    initials = initials.map(el => `${el[0]}.`);
    shortname = [lastname, ...initials].join(' ');
  }

  res.send(shortname);
});

app.get('/task2c', (req, res) => {
  const url = req.query.username;
  const re = new RegExp('@?(https?:)?(//)?(([a-z.])[^/]*/)?@?([a-zA-Z0-9._]*)', 'i');
  const username = url.match(re);
  res.send(`@${username[5]}`);
});

app.get('/task2d', (req, res) => {
  const color = req.query.color &&
    unescape(req.query.color.trim().toLowerCase());

  const colorPattern = new RegExp([
    '^(#?([0-9a-f]{3}|[0-9a-f]{6}))$', // hex pattern
    '^(rgb\\((( *([1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]) *),){2}( *([1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]) *)\\))', // rgb pattern
    '^(hsl\\((( *([1-2]?[0-9]?[0-9]|3[0-5][0-9]) *),)(( *([1-9]?[0-9]|100)% *),)( *([1-9]?[0-9]|100)% *)\\))$' // hsl pattern
  ].join('|'));

  if (colorPattern.test(color)) {
    return res.send(one(color).hex());
  }

  return res.send('Invalid color');
});

app.listen(3000, () => {
  console.log('On http://localhost:3000/');
});
